﻿using System;
using System.Data;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using ApiClient.Models;
using ApiClient.OAuth2;
using System.Data.SqlClient;

namespace DK_OAUTH_Service
{
    public partial class DKService : ServiceBase
        {

         ///  Timer to invoke refresh methods
        Timer timer = new Timer();

        /// <summary>
        /// Class Definition
        /// </summary>
        public DKService()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Called when service starts
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
                {
                    WriteToFile("DK OAUTH Service is started at " + DateTime.Now);
                    timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                    timer.Interval = (1000 * 60 * 15.0);  // ( 15 Mins )
                    timer.Enabled = true;
                    WriteToFile("DK OAUTH Service Called at " + DateTime.Now);
                    var task = Task.Run(async () => await RefreshToken());
        }

        /// <summary>
        ///     Called when service stops
        /// </summary>
        protected override void OnStop()        
        {
            WriteToFile("DK OAUTH Service is stopped at " + DateTime.Now);
        }

        /// <summary>
        /// Invokes method to refresh OAUTH2 Token from Digi-key
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            // SFTL-SR-005
            // Removed time filter so service runs 24/7
            // if (DateTime.Now.Hour >= 6 & DateTime.Now.Hour <= 18)
            // {
                WriteToFile("DK OAUTH Service recalled at " + DateTime.Now);
                var task = Task.Run(async () => await RefreshToken());
            // }
        }

        /// <summary>
        /// Records events to log
        /// </summary>
        /// <param name="Message"></param>
        private void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        /// <summary>
        /// Updates tokens in SQL
        /// </summary>
        /// <param name="settings"></param>
        private void WriteToSQL(ApiClientSettings settings)
        {
#if DEBUG
            string connectionString = @"Server=192.168.33.11; Database=DEVELOPMENT; Trusted_Connection=true; MultipleActiveResultSets=true;";
#else
            string connectionString = @"Server=192.168.33.9; Database=NORCOTT; Trusted_Connection=true; MultipleActiveResultSets=true;";
#endif
            var sql = @"UPDATE DKTokens 
                                SET ClientId = @ClientId, ClientSecret = @ClientSecret, RedirectUri = @RedirectUri,
                                AccessToken = @AccessToken, RefreshToken = @RefreshToken, ExpirationDateTime = @ExpirationDateTime
                                WHERE ID = 1";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add("@ClientId", SqlDbType.NVarChar).Value = settings.ClientId;
                        command.Parameters.Add("@ClientSecret", SqlDbType.NVarChar).Value = settings.ClientSecret;
                        command.Parameters.Add("@RedirectUri", SqlDbType.NVarChar).Value = settings.RedirectUri;
                        command.Parameters.Add("@AccessToken", SqlDbType.NVarChar).Value = settings.AccessToken;
                        command.Parameters.Add("@RefreshToken", SqlDbType.NVarChar).Value = settings.RefreshToken;
                        command.Parameters.Add("@ExpirationDateTime", SqlDbType.DateTime).Value = settings.ExpirationDateTime;
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                WriteToFile(e.Message);
            }
        }

        /// <summary>
        /// Main method to refresh OAUTH 2.0 Tokens
        /// </summary>
        /// <returns></returns>
        private async Task RefreshToken()
        {
            try
            {
                var settings = ApiClientSettings.CreateFromConfigFile();
                var oAuth2Service = new OAuth2Service(settings);
                var oAuth2AccessToken = new ApiClient.OAuth2.Models.OAuth2AccessToken();
                WriteToFile("Before call to refresh");
                WriteToFile(settings.ToString());

                // Let's refresh the token
                // Have 3 attempts
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                       oAuth2AccessToken = await oAuth2Service.RefreshTokenAsync();
                    }
                    catch (Exception)
                    {
                        WriteToFile("Error Getting Token");
                    }
                    // If failed then record in log file and continue attempts
                    if (oAuth2AccessToken is null || oAuth2AccessToken.IsError  || String.IsNullOrEmpty (oAuth2AccessToken.RefreshToken) || String.IsNullOrEmpty(oAuth2AccessToken.AccessToken)  || oAuth2AccessToken.ExpiresIn <= 0) {
                        WriteToFile("Attempting Retry after 30s: " + i.ToString());
                        // Wait for 30s for system
                        System.Threading.Thread.Sleep(30 * 1000);   
                        continue; 
                    } else
                    // We're good so exit loop
                    {
                        break;
                    }
                }
                // Final check of status
                if (oAuth2AccessToken is null || oAuth2AccessToken.IsError || String.IsNullOrEmpty(oAuth2AccessToken.RefreshToken) || String.IsNullOrEmpty(oAuth2AccessToken.AccessToken) || oAuth2AccessToken.ExpiresIn <= 0)
                {
                    // Current Refresh token is invalid or expired 
                    WriteToFile("Current Refresh token is invalid or expired ");
                    return;
                }

                // Reached this point so update DB & log file
                if (!String.IsNullOrEmpty(oAuth2AccessToken.AccessToken))
                        settings.UpdateAndSave(oAuth2AccessToken);

                WriteToFile("After call to refresh");
                WriteToFile(settings.ToString());

                WriteToFile("Write to SQL After call to refresh");
                if (!String.IsNullOrEmpty(oAuth2AccessToken.AccessToken))
                        WriteToSQL(settings);


                    }
            catch (System.Exception e)
            {
                WriteToFile("RefreshToken Method Failed: " + e.Message);
                throw;
            }
        }


        } // Class

  } // Namespace
